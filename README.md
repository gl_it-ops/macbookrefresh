# GitLab Employee Macbook Refresh Program

As part of our technology refresh process we have selected a group of users to recieve a new Macbook Pro system. While your existing system may be considered "new" we are testing alternate configurations and a new self service model from Apple. 

If you have recieved communication from IT-OPs via email please [click here](https://gitlab.company/refresh) to specificy your configuration and shipping options. 
